# PingApp

RESTful API интерфейс для сервера проверки связности сети. Сервис мониторинга доступности "адреса". При поступлении заказа от клиента "адрес" ставится на мониторинг. При окончании действия услуги "адрес" из мониторинга уходит. 

## Клонирование проекта

```
git clone https://borodulinairina1994@bitbucket.org/borodulinairina1994/pingapp.git
```

## Установка

### Необходимые пакеты

* [Java 8](http://www.oracle.com/technetwork/java/javase/downloads/jdk10-downloads-4416644.html) 
* [Gradle](https://gradle.org/) - Dependency Management
* [MongoDB](https://www.mongodb.com) - СУБД

## Сборка и запуск

```
$ cd PingApp/
$ sh gradlew clean build
$ sh gradlew bootRun
```

## Примеры использования

```
Запрос начала пинговки:
http://localhost:8080/add?url=https://habr.com/post/266375/
http://localhost:8080/add?url=https://yandex.ru/

Запрос для остановки пинговки:
http://localhost:8080/delete?url=https://habr.com/post/266375/ 
http://localhost:8080/delete?url=https://habr.com/
http://localhost:8080/delete?url=https://yandex.ru/

Отчет обо всех имеющихся сессиях мониторинга:
http://localhost:8080/admin

Запрос на все сессии:
http://localhost:8080/booker

```

## Authors

* **Бородулина И.А.** - [e-mail](borodulinairina1994@gmail.com)
