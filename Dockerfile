FROM gradle:4.6.0-jdk8-alpine
MAINTAINER irinaborodulina
ADD build/docker/pingapp-0.1.jar /jarFile/
WORKDIR '/jarFile'
CMD ["java", "-jar", "pingapp-0.1.jar"]
EXPOSE 8090

