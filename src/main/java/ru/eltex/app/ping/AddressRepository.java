package ru.eltex.app.ping;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Репозиторий адресов.
 */
public interface AddressRepository extends MongoRepository<Address, String> {
    /**
     * Поиск адреса по url.
     *
     * @param url
     * @return Объект Address
     */
    Address findAddressByUrl(String url);
}
