package ru.eltex.app.ping;

/**
 * Парсинг результатов пинговки.
 */
public class Parser {

    /**
     * Подсчет числа отправленных пакетов.
     *
     * @param text Результат пинговки
     * @return Число отправленных пакетов
     */
    public int counPack(String text) {
        return text.split("time=").length;
    }

    /**
     * Подсчет среднего времени.
     *
     * @param text Результат пинговки
     * @return Среднее время/-1 - ошибка пинга
     */
    public float avg(String text) {
        if (text.contains("time")) {
            String[] pack = text.split(" ms");
            Float summ = new Float(0);
            for (int i = 0; i < pack.length; i++) {
                String time = pack[i].split("time=")[1];
                summ += new Float(time);
            }
            return summ / pack.length;
        } else {
            return -1;
        }
    }
}
