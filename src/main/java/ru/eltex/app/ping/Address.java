package ru.eltex.app.ping;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;

/**
 * Хранит в себе информацию об отслеживаемом адресе.
 */
public class Address {

    @Id
    private String id;
    /**
     * Адрес ресурса
     */
    private String url;
    /**
     * Дата начала пинговки
     */
    private String startDate;
    /**
     * Дата конца пинговки
     */
    private String endDate;
    /**
     * Среднее время
     */
    private String avg;
    /**
     * Число пакетов
     */
    private String countPack;
    /**
     * Статус пинговки (активна/завершена)
     */
    private String status;
    /**
     * Объект - нить, в котором создается процесс пинговки адреса
     */
    @Transient
    @JsonIgnore
    private RunPing runPing;
    /**
     * id пользователя (зарезервировано)
     */
    private String userId;

    public Address() {
    }

    /**
     * @param url       Адрес добавленный для отслеживания
     * @param startDate Дата начала отслеживания
     * @param endDate   Дата окончания
     */
    public Address(String url, String startDate, String endDate) {
        this.url = url;
        this.startDate = startDate;
        this.endDate = endDate;
        this.runPing = new RunPing(url);
        runPing.start();
    }


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getAvg() {
        return avg;
    }

    public void setAvg(String avg) {
        this.avg = avg;
    }

    public String getCountPack() {
        return countPack;
    }

    public void setCountPack(String countPack) {
        this.countPack = countPack;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public RunPing getRunPing() {
        return runPing;
    }

    @Override
    public String toString() {
        return "Address{" +
                "id='" + id + '\'' +
                ", url='" + url + '\'' +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                '}';
    }
}
