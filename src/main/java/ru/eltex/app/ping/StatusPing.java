package ru.eltex.app.ping;

/**
 * Статус пинговки (пинговка в данный момент активна. Пинговка завершена).
 */
public enum StatusPing {
    /**
     * Активно
     */
    ACTIVE,
    /**
     * Неактивно
     */
    INACTIVE
}
