package ru.eltex.app.ping;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PingLogger {

    private Logger log;

    public PingLogger() {
        log = LogManager.getLogger(PingLogger.class);
    }

    public void info(String s){
        log.info(s);
    }
}
