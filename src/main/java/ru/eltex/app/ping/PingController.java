package ru.eltex.app.ping;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;

/**
 * Контроллер.
 */
@RestController
public class PingController {
    PingLogger pingLogger = new PingLogger();
    /**
     * Адреса, которые в данный момент пингуются
     * key - url, value - Объект Address.
     */
    static HashMap<String, Address> addresses = new HashMap<>();

    /**
     * Репозиторий адресов
     */
    @Autowired
    private AddressRepository addressRepository;

    @RequestMapping(value = "/")
    public String index() {
        return "Система пинговки";
    }

    /**
     * Запрос для начала пинговки.
     *
     * @param url Адрес
     * @return Объект Address
     */
    @RequestMapping(params = {"url"}, value = "/add", method = RequestMethod.GET)
    Address addAddress(@RequestParam("url") String url) {
        URL host = this.valideURL(url);
        if (host != null) {
            if (!addresses.containsKey(host.getHost())) {
                url = host.getHost();
                Address address = new Address(url, System.currentTimeMillis() + "", System.currentTimeMillis() + "");
                address.setStatus(StatusPing.ACTIVE.toString());
                address = addressRepository.save(address);
                address.setStatus(StatusPing.ACTIVE.toString());
                addresses.put(url, address);
                pingLogger.info("add "+url);
            return address;
        } }
        return null;
    }

    /**
     * Запрос для остановки пинговки.
     *
     * @param url Адрес
     * @return Статус активности пинговки, колличество посланных пакетов, среднее время
     */
    @RequestMapping(params = {"url"}, value = "/delete", method = RequestMethod.GET)
    String deleteAddress(@RequestParam("url") String url) {
        URL host = this.valideURL(url);
        if (host != null) {
            Address address = addresses.get(host.getHost());
            if (address != null && host != null) {
                url = host.getHost();
                String result = addresses.get(url).getRunPing().stopPing();
                Parser parser = new Parser();
                float avg = parser.avg(result);
                int countPack = parser.counPack(result);
                address.setEndDate(System.currentTimeMillis() + "");
                address.setCountPack(countPack + "");
                address.setAvg(avg + "");
                address.setStatus(StatusPing.INACTIVE.toString());
                addresses.remove(host.getPath());
                addressRepository.save(address);
                if (avg > 0) {
                    return "Среднее время: " + avg + ". Число отправленных пакетов: " + countPack + " Статус: " + address.getStatus();
                } else {
                    logger.info("Нет ответа " + url);
                    return "Нет ответа " + url;//логи
                }
            }
        }
            logger.info("Адрес не пингуется/неверный " + url);
            return "Адрес не пингуется/неверный " + url;
    }

    /**
     * Проверка статуса пинговки адреса.
     *
     * @param url Адрес
     * @return Статус пинговки
     */
    @RequestMapping(params = {"url"}, value = "/status", method = RequestMethod.GET)
    String statusPing(@RequestParam("url") String url) {
        Address address = addresses.get(url);
        if (address != null) {
            return address.getStatus();
        } else
            return StatusPing.INACTIVE.toString();
    }

    /**
     * Страница администратора. Отчет обо всех имеющихся сессиях мониторинга.
     *
     * @return Таблица всех пингующихся адресов
     */
    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    HashMap<String, Address> adminPage() {
        return addresses;
    }

    /**
     * Страница бухгалтера. Выводятся все сессии.
     *
     * @return Список всех адресов
     */
    @RequestMapping(value = "/booker")
    List<Address> bookerPage() {
        return addressRepository.findAll();
    }

    /**
     * Проверка url на валидность.
     *
     * @param url Адрес
     * @return Валиден/невалиден
     */
    private URL valideURL(String url) {
        try {
            return new URL(url);
        } catch (MalformedURLException e) {
            logger.error("Неверный URL: " + url);
            return null;
        }
    }
}
