package ru.eltex.app.ping;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Создается процесс пинговки в отдельном потоке.
 */
public class RunPing extends Thread {
    /**
     * Адрес ресурса
     */
    private String url;
    /**
     * Процесс пинговки
     */
    private Process process;
    /**
     * Результат пинговки
     */
    private StringBuilder result;
    /**
     * Логгер
     */
    private PingLogger logger = new PingLogger();

    /**
     * @param url Адрес для пинговки
     */
    public RunPing(String url) {
        this.url = url;
    }

    /**
     * Создается процесс пинговки адреса. Ошибки логируются
     */
    @Override
    public void run() {
        try {
            process = Runtime.getRuntime().exec("ping " + url);
        } catch (IOException e) {
            logger.error("Ошибка при создании процесса пинговки адреса: " + url + "\n" + e.toString());
        }
        try (BufferedReader inputStream = new BufferedReader(
                new InputStreamReader(process.getInputStream()))) {
            logger.info("Создание пинга: " + url + "\n");
            String s = "";
            result = new StringBuilder();
            while ((s = inputStream.readLine()) != null) {
                result.append(s);
            }
        } catch (IOException e) {
            logger.error("Ошибка при пинговке адреса: " + url + "\n" + e.toString());
        }
    }

    /**
     * Остановка отслеживания адреса
     *
     * @return возвращается строка результата пинговки
     */
    public String stopPing() {
        process.destroy();
        logger.info("Остановка пинга: " + url + "\n");
        return result.toString();
    }
}
